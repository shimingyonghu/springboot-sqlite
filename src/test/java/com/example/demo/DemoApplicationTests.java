package com.example.demo;


import com.example.demo.dao.UsersDao;
import com.example.demo.domain.Users;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;



@SpringBootTest
@RunWith(SpringRunner.class)
public class DemoApplicationTests {

    @Autowired
    private UsersDao usersDao;

    @Test
    public void contextLoads() {
        Users users = new Users();
        users.setUsername("*/5 * * * * ?");
        usersDao.save(users);
    }

}
