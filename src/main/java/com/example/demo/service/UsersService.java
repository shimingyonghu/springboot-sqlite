package com.example.demo.service;

import com.example.demo.dao.UsersDao;
import com.example.demo.domain.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class UsersService {

    @Autowired
    private UsersDao usersDao;

    public Optional<Users> findById(int id){
        return usersDao.findById(id);
    }

    public void save(Users users){
        System.out.println(users.getUsername());
        usersDao.saveAndFlush(users);
    }

    public List<String> findAll(int id){
        Optional<Users> optional = usersDao.findById(id);
        Users users = optional.get();
        String username = users.getUsername();
        System.out.println(username);
        File file = new File(username);
        File[] files = file.listFiles();
        List<String> list = new LinkedList<>();
        for (int i = 0; i < files.length; i++) {
            list.add(files[i].getName());
        }
        return list;
    }

}
