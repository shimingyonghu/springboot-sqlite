package com.example.demo.controller;


import com.example.demo.domain.Users;
import com.example.demo.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UsersController {

    @Autowired
    private UsersService usersService;

    @PostMapping("/add")
    public void save(@RequestBody Users users){
        System.out.println(users.getUsername());
        usersService.save(users);
    }

    @GetMapping("list")
    public List<String> findById(@Param("id") int id){
        return usersService.findAll(id);
    }

}
